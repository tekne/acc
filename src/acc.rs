use std::ptr::NonNull;
use super::data::*;
use super::count::*;
use super::trace::*;

/// A cycle-counted reference to `T` which can be shared across threads
pub struct Acc<T: Trace, C: AtomicCount> {
    /// The underlying pointer type
    ptr: NonNull<CcData<T, C>>
}
