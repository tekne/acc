use super::trace::*;
use super::count::*;

/// A cycle-counted object of type `T`
pub struct CcData<T: Trace, C: Count> {
    /// The reference counts
    refs: C,
    /// The underlying data
    data: T
}
