/// A trait indicating the kind of cyclic references which a type can hold
pub trait CycleKind {}

/// A marker struct indicating a type is acyclic, i.e. cannot hold cyclic references to itself
/// Incorrectly marking a type as acyclic can cause memory leaks, but never memory unsafety.
#[derive(Debug, Copy, Clone)]
pub struct Acyclic;
impl CycleKind for Acyclic {}

/// A marker struct indicating a type is cyclic, i.e. can hold cyclic references to itself.
#[derive(Debug, Copy, Clone)]
pub struct Cyclic;
impl CycleKind for Cyclic {}

/// A trait implemented by tracers
pub trait Tracer {}

/// A trait implemented by types which can be traced for cyclic references
pub trait Trace {
    type IsCyclic: CycleKind;
    fn trace<T: Tracer>(&self, tracer: &mut T);
}
