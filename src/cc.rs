use std::ptr::NonNull;
use super::data::*;
use super::count::*;
use super::trace::*;

/// A cycle-counted reference to `T` which cannot be shared across threads
pub struct Cc<T: Trace, C: Count> {
    /// The underlying pointer type
    ptr: NonNull<CcData<T, C>>
}
