use std::cell::Cell;

/// A trait implemented by a type of reference count
pub unsafe trait Count {
    /// Increment this reference count by n
    fn increment(&self, n: usize) -> usize;
    /// Decrement this reference count by n. Return the new count
    fn decrement(&self, n: usize) -> usize;
    /// Get the current reference count
    fn strong(&self) -> usize;
}

/// A trait implemented by a type of reference count supporting weak references
pub unsafe trait WeakCount: Count {
    /// Increment this weak reference count by n
    fn increment_weak(&self, n: usize) -> usize;
    /// Decrement this weak reference count by n
    fn decrement_weak(&self, n: usize) -> usize;
    /// Get the current weak reference count
    fn weak(&self) -> usize;
}

/// The state of a cycle collected object for Bacon-Rajan cycle collection
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum StateColor {
    /// An object which is in use or free
    Black = 0,
    /// A possible member of a cycle
    Gray = 1,
    /// A member of a garbage cycle
    White = 2,
    /// A possible root of a cycle
    Purple = 3,
    /// An acyclic node
    Green = 4,
    /// A candidate cycle undergoing Sigma-computation
    Red = 5,
    /// A candidate cycle awaiting the epoch boundary
    Orange = 6
}


/// A trait implemented by a type of reference count supporting Bacon-Rajan cycle collection
pub unsafe trait BaconRajanCycleCount: Count {
    /// Get the current color
    fn color(&self) -> StateColor;
    /// Get whether this node is buffered
    fn buffered(&self) -> bool;
    /// Set the color for this node
    fn set_color(&self, color: StateColor);
    /// Set this node as buffered
    fn set_buffered(&self, buffered: bool);
}

/// A trait implemented by an atomic type of reference count
pub unsafe trait AtomicCount: Count {}

/// A simple, thread-local reference count supporting both weak and strong references
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct SimpleCount {
    /// The weak reference count
    weak: Cell<usize>,
    /// The strong reference count
    strong: Cell<usize>,
    /// The color
    color: Cell<StateColor>,
    /// Whether this state is buffered
    buffered: Cell<bool>
}

unsafe impl Count for SimpleCount {
    #[inline(always)] fn increment(&self, n: usize) -> usize {
        if n != 0 {
            self.strong.set(self.strong.get() + n);
            self.color.set(StateColor::Black)
        }
        self.strong.get()
    }
    #[inline(always)] fn decrement(&self, n: usize) -> usize {
        if n != 0 {
            self.strong.set(self.strong.get().saturating_sub(n));
            self.color.set(StateColor::Gray)
        }
        self.strong.get()
    }
    #[inline(always)] fn strong(&self) -> usize { self.strong.get() }
}

unsafe impl WeakCount for SimpleCount {
    #[inline(always)] fn increment_weak(&self, n: usize) -> usize {
        self.weak.set(self.weak.get() + n);
        self.weak.get()
    }
    #[inline(always)] fn decrement_weak(&self, n: usize) -> usize {
        self.weak.set(self.weak.get().saturating_sub(n));
        self.weak.get()
    }
    #[inline(always)] fn weak(&self) -> usize { self.weak.get() }
}

unsafe impl BaconRajanCycleCount for SimpleCount {
    #[inline(always)] fn color(&self) -> StateColor { self.color.get() }
    #[inline(always)] fn buffered(&self) -> bool { self.buffered.get() }
    #[inline(always)] fn set_color(&self, color: StateColor) { self.color.set(color) }
    #[inline(always)] fn set_buffered(&self, buffered: bool) { self.buffered.set(buffered) }
}
